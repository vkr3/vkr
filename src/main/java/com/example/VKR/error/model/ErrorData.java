package com.example.VKR.error.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class ErrorData {
    private HttpStatus code;
    private String message;
}
