package com.example.VKR.error.handler;

import com.example.VKR.error.model.ErrorData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorData> handleException(Exception e, WebRequest request) {
        return ResponseEntity
                .badRequest()
                .body(new ErrorData(HttpStatus.BAD_REQUEST, e.getMessage()));
    }
}
