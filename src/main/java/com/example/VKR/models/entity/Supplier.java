package com.example.VKR.models.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private int deliveryPrise;
    private int deliveryDays;

    @OneToMany(mappedBy = "supplier")
    private List<SupplierInfo> supplierInfos;
}
