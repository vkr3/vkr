package com.example.VKR.models.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@ToString(exclude = "supplierInfos")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private ProductType type;

    @OneToOne (cascade = CascadeType.ALL)
    private Quantity quantity;

    @OneToMany(mappedBy = "product")
    private List<SupplierInfo> supplierInfos;

    public enum ProductType {
        CHEMICALS("Химикаты"),
        BIO_PROTECTION("Био.защита"),
        FERTILIZERS("Удобрение");

        private String name;

        ProductType(String name) {
            this.name = name;
        }

        @JsonValue
        public String getName() {
            return name;
        }
    }
}
