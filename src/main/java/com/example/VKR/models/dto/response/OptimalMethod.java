package com.example.VKR.models.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
public class OptimalMethod {
    private String productName;
    private double productPrice;
    private double optimalOrder;
    private double discountBorder;
    private double discountPrice;
    private double expensesIndicatorWithDiscount;
    private double expensesIndicatorWithoutDiscount;
    private OffsetDateTime optimalOrderDate;
}
