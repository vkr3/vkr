package com.example.VKR.models.dto;

import com.example.VKR.models.entity.Role;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {
    private int id;
    private String username;
    private String token;
    private String email;
    private String password;
    private Role role;
}
