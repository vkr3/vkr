package com.example.VKR.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SupplierInfoDTO {
    private int id;
    private int supplierPrice;
    private int discountPrice;
    private int discountBorder;
    private SupplierDTO supplier;
    private ProductDTO product;
}
