package com.example.VKR.models.dto;

import com.example.VKR.models.entity.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ProductDTO {
    private int id;
    private String name;
    private Product.ProductType type;
    private QuantityDTO quantity;
    private List<SupplierInfoDTO> supplierInfos;
}
