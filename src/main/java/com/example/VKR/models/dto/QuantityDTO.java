package com.example.VKR.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QuantityDTO {
    private int id;
    private int price;
    private String units;
    private int expenses;
}
