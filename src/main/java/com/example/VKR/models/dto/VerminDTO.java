package com.example.VKR.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class VerminDTO {
    private int id;
    private String name;
    private List<ProductDTO> products;
}
