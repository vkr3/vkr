package com.example.VKR.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SupplierDTO {
    private int id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private int deliveryPrise;
    private int deliveryDays;
    private List<SupplierInfoDTO> supplierInfos;
}
