package com.example.VKR.controller;

import com.example.VKR.converter.Converter;
import com.example.VKR.models.dto.SupplierInfoDTO;
import com.example.VKR.models.entity.SupplierInfo;
import com.example.VKR.service.SupplierInfoService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/supplierInfo")
public class SupplierInfoController {
    private final Converter<SupplierInfo, SupplierInfoDTO> converter;
    private final SupplierInfoService supplierInfoService;

    public SupplierInfoController(Converter<SupplierInfo, SupplierInfoDTO> converter,
                                  SupplierInfoService supplierInfoService) {
        this.supplierInfoService = supplierInfoService;
        this.converter = converter;
    }

    @PostMapping
    public SupplierInfoDTO createSupplierInfo(@RequestBody SupplierInfoDTO supplierInfoDTO) {
        return converter.toDTO(
                supplierInfoService.create(
                        converter.toEntity(supplierInfoDTO)
                )
        );
    }
}
