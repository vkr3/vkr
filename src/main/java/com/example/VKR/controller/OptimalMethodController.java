package com.example.VKR.controller;


import com.example.VKR.service.OptimalMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/calculator")
public class OptimalMethodController {

    @Autowired
    private OptimalMethodService optimalMethodService;

    @PostMapping("/optimalMethod")
    private Object calculateOptimalMethod() {
        return optimalMethodService.calculateOptimalMethod();
    }
}
