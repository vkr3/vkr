package com.example.VKR.controller;

import com.example.VKR.converter.Converter;
import com.example.VKR.models.dto.VerminDTO;
import com.example.VKR.models.entity.Vermin;
import com.example.VKR.service.VerminService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/vermin")
public class VerminController {
    public final VerminService verminService;
    public final Converter<Vermin, VerminDTO> converter;

    public VerminController(VerminService verminService,
                            Converter<Vermin, VerminDTO> converter) {
        this.verminService = verminService;
        this.converter = converter;
    }

    @PostMapping
    public VerminDTO createVermin(@RequestBody VerminDTO verminDTO) {
        return converter.toDTO(
                verminService.create(
                        converter.toEntity(verminDTO)
                )
        );
    }

    @GetMapping
    public List<VerminDTO> getVermins() {
        return verminService.findAll().stream()
                .map(converter::toDTO)
                .collect(Collectors.toList());
    }
}
