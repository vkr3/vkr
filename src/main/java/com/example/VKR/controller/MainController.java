package com.example.VKR.controller;

import com.example.VKR.models.entity.Text;
import com.example.VKR.repository.TextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@RequestMapping
public class MainController {
    private final TextRepository textRepository;

    public MainController(TextRepository textRepository) {
        this.textRepository = textRepository;
    }

    @GetMapping
    public String main(Map<String,Object> model)
    {
      Iterable<Text> text = textRepository.findAll();
      model.put("text",text);
        return "main";

    }

    @GetMapping("/hello")
    public String hello(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "hello";
    }

    @PostMapping
    public String add(@RequestParam String text,
                      @RequestParam String tag,
                      Map<String,Object> model){
        
        return "main";
    }
}
