package com.example.VKR.controller;


import com.example.VKR.converter.Converter;
import com.example.VKR.models.dto.UserDTO;
import com.example.VKR.models.entity.User;
import com.example.VKR.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
public class UserController {
    private final UserService userService;
    private final Converter<User, UserDTO> converter;

    public UserController(UserService userService,
                          Converter<User, UserDTO> converter) {
        this.userService = userService;
        this.converter = converter;
    }

    @PostMapping("/register")
    public UserDTO register(@RequestBody UserDTO userDTO) {
        return converter.toDTO(
                userService.register(
                        converter.toEntity(userDTO)
                )
        );
    }

    @PostMapping("/login")
    public UserDTO login(@RequestBody UserDTO userDTO) {
        return converter.toDTO(
                userService.login(
                        converter.toEntity(userDTO)
                )
        );
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable Integer id) {
        userService.delete(id);
    }
}
