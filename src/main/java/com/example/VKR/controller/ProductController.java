package com.example.VKR.controller;

import com.example.VKR.converter.Converter;
import com.example.VKR.models.dto.ProductDTO;
import com.example.VKR.models.entity.Product;
import com.example.VKR.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/product")
public class ProductController {

    private final ProductService productService;
    private final Converter<Product, ProductDTO> converter;

    public ProductController(ProductService productService,
                             Converter<Product, ProductDTO> converter) {
        this.productService = productService;
        this.converter = converter;
    }

    @PostMapping
    public ProductDTO create(@RequestBody ProductDTO productDTO) {
        return converter.toDTO(
                productService.create(
                        converter.toEntity(productDTO)
                )
        );
    }

    @GetMapping
    public List<ProductDTO> getProductDTO() {
        return productService.getProducts().stream()
                .map(converter::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/search")
    public List<ProductDTO> search(@RequestParam Product.ProductType type) {
        return productService.search(type).stream()
                .map(converter::toDTO)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        productService.delete(id);
    }
}
