package com.example.VKR.controller;


import com.example.VKR.converter.Converter;
import com.example.VKR.models.dto.SupplierDTO;
import com.example.VKR.models.entity.Supplier;
import com.example.VKR.service.SupplierService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/supplier")
public class SupplierController {

    private final SupplierService supplierService;
    private final Converter<Supplier, SupplierDTO> converter;

    public SupplierController(SupplierService supplierService, Converter<Supplier, SupplierDTO> converter) {
        this.supplierService = supplierService;
        this.converter = converter;
    }

    @PostMapping
    public SupplierDTO create(@RequestBody SupplierDTO supplierDTO) {
        return converter.toDTO(
                supplierService.create(
                        converter.toEntity(supplierDTO)
                )
        );
    }

    @GetMapping
    public List<SupplierDTO> getSuppliers() {
        return supplierService.getSuppliers().stream()
                .map(converter::toDTO)
                .collect(Collectors.toList());
    }
}
