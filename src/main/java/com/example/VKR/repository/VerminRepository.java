package com.example.VKR.repository;

import com.example.VKR.models.entity.Vermin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerminRepository extends JpaRepository<Vermin, Integer> {
}
