package com.example.VKR.repository;

import com.example.VKR.models.entity.SupplierInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierInfoRepository extends JpaRepository<SupplierInfo, Integer> {
}
