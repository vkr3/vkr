package com.example.VKR.repository;


import com.example.VKR.models.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//репозиторий для управление классом user
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    boolean existsByUsername(String username);

    Optional<User> findByUsernameAndPassword(String username, String password);

}
