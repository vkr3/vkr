package com.example.VKR.repository;
import com.example.VKR.models.entity.Text;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TextRepository extends CrudRepository<Text,Long> {
}
