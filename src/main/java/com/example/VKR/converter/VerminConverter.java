package com.example.VKR.converter;

import com.example.VKR.models.dto.ProductDTO;
import com.example.VKR.models.dto.VerminDTO;
import com.example.VKR.models.entity.Product;
import com.example.VKR.models.entity.Vermin;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class VerminConverter implements Converter<Vermin, VerminDTO> {
    private final Converter<Product, ProductDTO> converter;

    public VerminConverter(Converter<Product, ProductDTO> converter) {
        this.converter = converter;
    }

    @Override
    public VerminDTO toDTO(Vermin entity) {
        VerminDTO verminDTO = new VerminDTO();
        verminDTO.setId(entity.getId());
        verminDTO.setName(entity.getName());
        if (entity.getProducts() != null) {
            verminDTO.setProducts(entity.getProducts().stream()
                    .map(converter::toDTO)
                    .collect(Collectors.toList()));
        }
        return verminDTO;
    }

    @Override
    public Vermin toEntity(VerminDTO entity) {
        Vermin vermin = new Vermin();
        vermin.setId(entity.getId());
        vermin.setName(entity.getName());
        if (entity.getProducts() != null) {
            vermin.setProducts(entity.getProducts().stream()
                    .map(converter::toEntity)
                    .collect(Collectors.toList()));
        }
        return vermin;
    }
}
