package com.example.VKR.converter;

import com.example.VKR.models.dto.ProductDTO;
import com.example.VKR.models.dto.SupplierDTO;
import com.example.VKR.models.dto.SupplierInfoDTO;
import com.example.VKR.models.entity.Product;
import com.example.VKR.models.entity.Supplier;
import com.example.VKR.models.entity.SupplierInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SupplierInfoConverter implements Converter<SupplierInfo, SupplierInfoDTO> {
    @Autowired
    private Converter<Product, ProductDTO> productConverter;
    @Autowired
    private Converter<Supplier, SupplierDTO> supplierConverter;

    @Override
    public SupplierInfoDTO toDTO(SupplierInfo entity) {
        SupplierInfoDTO supplierInfoDTO = new SupplierInfoDTO();
        supplierInfoDTO.setId(entity.getId());
        supplierInfoDTO.setDiscountBorder(entity.getDiscountBorder());
        supplierInfoDTO.setDiscountPrice(entity.getDiscountPrice());
        if (entity.getProduct() != null) {
            supplierInfoDTO.setProduct(productConverter.toDTO(entity.getProduct()));
        }
        if (entity.getSupplier() != null) {
            supplierInfoDTO.setSupplier(supplierConverter.toDTO(entity.getSupplier()));
        }
        supplierInfoDTO.setSupplierPrice(entity.getSupplierPrice());
        return supplierInfoDTO;
    }

    @Override
    public SupplierInfo toEntity(SupplierInfoDTO dto) {
        SupplierInfo supplierInfo = new SupplierInfo();
        supplierInfo.setId(dto.getId());
        supplierInfo.setDiscountBorder(dto.getDiscountBorder());
        supplierInfo.setDiscountPrice(dto.getDiscountPrice());
        if (dto.getProduct() != null) {
            supplierInfo.setProduct(productConverter.toEntity(dto.getProduct()));
        }
        if (dto.getSupplier() != null) {
            supplierInfo.setSupplier(supplierConverter.toEntity(dto.getSupplier()));
        }
        supplierInfo.setSupplierPrice(dto.getSupplierPrice());
        return supplierInfo;
    }
}
