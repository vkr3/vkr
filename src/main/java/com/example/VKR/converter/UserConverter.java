package com.example.VKR.converter;

import com.example.VKR.models.dto.UserDTO;
import com.example.VKR.models.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter<User, UserDTO> {
    @Override
    public UserDTO toDTO(User entity) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(entity.getUsername());
        userDTO.setPassword(entity.getPassword());
        userDTO.setEmail(entity.getEmail());
        userDTO.setId(entity.getId());
        userDTO.setRole(entity.getRole());
        return userDTO;
    }

    @Override
    public User toEntity(UserDTO entity) {
        User user = new User();
        user.setUsername(entity.getUsername());
        user.setPassword(entity.getPassword());
        user.setEmail(entity.getEmail());
        user.setId(entity.getId());
        user.setRole(entity.getRole());
        return user;
    }
}
