package com.example.VKR.converter;

import com.example.VKR.models.dto.ProductDTO;
import com.example.VKR.models.dto.QuantityDTO;
import com.example.VKR.models.dto.SupplierInfoDTO;
import com.example.VKR.models.entity.Product;
import com.example.VKR.models.entity.Quantity;
import com.example.VKR.models.entity.SupplierInfo;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ProductConverter implements Converter<Product, ProductDTO> {
    private final Converter<Quantity, QuantityDTO> quantityConverter;

    public ProductConverter(Converter<Quantity, QuantityDTO> converter) {
        this.quantityConverter = converter;
    }

    @Override
    public ProductDTO toDTO(Product entity) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(entity.getId());
        productDTO.setName(entity.getName());
        if (entity.getQuantity() != null) {
            productDTO.setQuantity(quantityConverter.toDTO(entity.getQuantity()));
        }
        productDTO.setType(entity.getType());
        if (entity.getSupplierInfos() != null) {
            productDTO.setSupplierInfos(entity.getSupplierInfos().stream()
                    .map(supplierInfo -> {
                        SupplierInfoDTO supplierInfoDTO = new SupplierInfoDTO();
                        supplierInfoDTO.setSupplierPrice(supplierInfo.getSupplierPrice());
                        supplierInfoDTO.setDiscountPrice(supplierInfo.getDiscountPrice());
                        supplierInfoDTO.setDiscountBorder(supplierInfo.getDiscountBorder());
                        supplierInfoDTO.setId(supplierInfo.getId());
                        return supplierInfoDTO;
                    })
                    .collect(Collectors.toList()));
        }
        return productDTO;
    }

    @Override
    public Product toEntity(ProductDTO entity) {
        Product product = new Product();
        product.setId(entity.getId());
        product.setName(entity.getName());
        if (entity.getQuantity() != null) {
            product.setQuantity(quantityConverter.toEntity(entity.getQuantity()));
        }
        product.setType(entity.getType());
        if (entity.getSupplierInfos() != null) {
            product.setSupplierInfos(entity.getSupplierInfos().stream()
                    .map(supplierInfoDTO -> {
                        SupplierInfo supplierInfo = new SupplierInfo();
                        supplierInfo.setId(supplierInfoDTO.getId());
                        supplierInfo.setSupplierPrice(supplierInfoDTO.getSupplierPrice());
                        supplierInfo.setDiscountPrice(supplierInfoDTO.getDiscountPrice());
                        supplierInfo.setDiscountBorder(supplierInfo.getDiscountBorder());
                        return supplierInfo;
                    })
                    .collect(Collectors.toList()));
        }
        return product;
    }
}
