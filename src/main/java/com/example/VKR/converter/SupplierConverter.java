package com.example.VKR.converter;

import com.example.VKR.models.dto.ProductDTO;
import com.example.VKR.models.dto.SupplierDTO;
import com.example.VKR.models.dto.SupplierInfoDTO;
import com.example.VKR.models.entity.Product;
import com.example.VKR.models.entity.Supplier;
import com.example.VKR.models.entity.SupplierInfo;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class SupplierConverter implements Converter<Supplier, SupplierDTO> {

    @Override
    public SupplierDTO toDTO(Supplier entity) {
        SupplierDTO supplierDTO = new SupplierDTO();
        supplierDTO.setId(entity.getId());
        supplierDTO.setAddress(entity.getAddress());
        supplierDTO.setEmail(entity.getEmail());
        supplierDTO.setName(entity.getName());
        supplierDTO.setPhone(entity.getPhone());
        supplierDTO.setDeliveryDays(entity.getDeliveryDays());
        supplierDTO.setDeliveryPrise(entity.getDeliveryPrise());
        if (entity.getSupplierInfos() != null) {
            supplierDTO.setSupplierInfos(entity.getSupplierInfos().stream()
                    .map(supplierInfo -> {
                        SupplierInfoDTO supplierInfoDTO = new SupplierInfoDTO();
                        supplierInfoDTO.setSupplierPrice(supplierInfo.getSupplierPrice());
                        supplierInfoDTO.setDiscountPrice(supplierInfo.getDiscountPrice());
                        supplierInfoDTO.setDiscountBorder(supplierInfo.getDiscountBorder());
                        supplierInfoDTO.setId(supplierInfo.getId());
                        Product supplierInfoProduct = supplierInfo.getProduct();
                        if (supplierInfoProduct != null) {
                            ProductDTO product = new ProductDTO();
                            product.setName(supplierInfoProduct.getName());
                            product.setType(supplierInfoProduct.getType());
                            product.setId(supplierInfoProduct.getId());
                            supplierInfoDTO.setProduct(product);
                        }
                        return supplierInfoDTO;
                    })
                    .collect(Collectors.toList()));
        }
        return supplierDTO;
    }

    @Override
    public Supplier toEntity(SupplierDTO entity) {
        Supplier supplier = new Supplier();
        supplier.setId(entity.getId());
        supplier.setAddress(entity.getAddress());
        supplier.setEmail(entity.getEmail());
        supplier.setName(entity.getName());
        supplier.setPhone(entity.getPhone());
        supplier.setDeliveryDays(entity.getDeliveryDays());
        supplier.setDeliveryPrise(entity.getDeliveryPrise());
        if (entity.getSupplierInfos() != null) {
            supplier.setSupplierInfos(entity.getSupplierInfos().stream()
                    .map(supplierInfoDTO -> {
                        SupplierInfo supplierInfo = new SupplierInfo();
                        supplierInfo.setId(supplierInfoDTO.getId());
                        supplierInfo.setSupplierPrice(supplierInfoDTO.getSupplierPrice());
                        supplierInfo.setDiscountPrice(supplierInfoDTO.getDiscountPrice());
                        supplierInfo.setDiscountBorder(supplierInfo.getDiscountBorder());
                        return supplierInfo;
                    })
                    .collect(Collectors.toList()));
        }
        return supplier;
    }
}
