package com.example.VKR.converter;

public interface Converter<E, D> {
    D toDTO(E entity); // Конвертирует в ДТО
    E toEntity(D entity); // Конвертирует в энтити
}
