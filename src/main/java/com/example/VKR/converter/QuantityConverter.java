package com.example.VKR.converter;

import com.example.VKR.models.dto.QuantityDTO;
import com.example.VKR.models.entity.Quantity;
import org.springframework.stereotype.Component;

@Component
public class QuantityConverter implements Converter<Quantity, QuantityDTO> {
    @Override
    public QuantityDTO toDTO(Quantity entity) {
        QuantityDTO quantity = new QuantityDTO();
        quantity.setId(entity.getId());
        quantity.setExpenses(entity.getExpenses());
        quantity.setPrice(entity.getPrice());
        quantity.setUnits(entity.getUnits());
        return quantity;
    }

    @Override
    public Quantity toEntity(QuantityDTO entity) {
        Quantity quantity = new Quantity();
        quantity.setId(entity.getId());
        quantity.setExpenses(entity.getExpenses());
        quantity.setPrice(entity.getPrice());
        quantity.setUnits(entity.getUnits());
        return quantity;
    }
}
