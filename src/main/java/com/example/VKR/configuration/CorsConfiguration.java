package com.example.VKR.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//класс может быть использован контейнером Spring IoC как конфигурационный класс для бинов
@Configuration
public class CorsConfiguration {
    //класс, который управляется контейнером Spring
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            //переопределение
            @Override
            //дать возможность хосту 3000 взаимодействовать с бэкендом
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:3000")
                        .allowedMethods("PUT", "POST", "PATCH", "CREATE", "DELETE", "GET");
            }
        };
    }
}
