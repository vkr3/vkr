package com.example.VKR.service;


import com.example.VKR.models.entity.Product;
import com.example.VKR.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product create(Product product) {
        return productRepository.save(product);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public List<Product> search(Product.ProductType type) {
        return productRepository.findAllByType(type);
    }

    public void delete(Integer id) {
        productRepository.deleteById(id);
    }
}
