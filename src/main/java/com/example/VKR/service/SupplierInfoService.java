package com.example.VKR.service;

import com.example.VKR.models.entity.SupplierInfo;
import com.example.VKR.repository.SupplierInfoRepository;
import org.springframework.stereotype.Service;

@Service
public class SupplierInfoService {
    private final SupplierInfoRepository supplierInfoRepository;

    public SupplierInfoService(SupplierInfoRepository supplierInfoRepository) {
        this.supplierInfoRepository = supplierInfoRepository;
    }

    public SupplierInfo create(SupplierInfo supplierInfo) {
        return supplierInfoRepository.save(supplierInfo);
    }
}
