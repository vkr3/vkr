package com.example.VKR.service;

import com.example.VKR.models.entity.Vermin;
import com.example.VKR.repository.VerminRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VerminService {
    private final VerminRepository verminRepository;

    public VerminService(VerminRepository verminRepository) {
        this.verminRepository = verminRepository;
    }

    public List<Vermin> findAll() {
        return verminRepository.findAll();
    }

    public Vermin create(Vermin vermin) {
        return verminRepository.save(vermin);
    }
}
