package com.example.VKR.service;


import com.example.VKR.models.entity.User;
import com.example.VKR.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.security.AccessControlException;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User register(User user) {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new AccessControlException(String.format("Пользователь с именем %s уже существует", user.getUsername()));
        }
        return userRepository.save(user);
    }

    public User login(User user) {
        return userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword())
                .orElseThrow(() -> new AccessControlException(String.format("Пользователь с именем %s не зарегестрирован", user.getUsername())));
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }
}