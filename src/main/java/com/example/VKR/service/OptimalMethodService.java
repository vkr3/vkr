package com.example.VKR.service;

import com.example.VKR.models.dto.response.OptimalMethod;
import com.example.VKR.models.entity.Product;
import com.example.VKR.models.entity.Quantity;
import com.example.VKR.models.entity.Supplier;
import com.example.VKR.models.entity.SupplierInfo;
import com.example.VKR.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

//Suppliers
//Product.quantity
//Product.suppliersInfo

@Service
public class OptimalMethodService {

    private final ProductRepository productRepository;

    public OptimalMethodService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<OptimalMethod> calculateOptimalMethod() {
        List<OptimalMethod> result = new ArrayList<>();

        List<Product> products = productRepository.findAllByType(Product.ProductType.FERTILIZERS);

        for (Product product : products) {
            List<SupplierInfo> supplierInfos = product.getSupplierInfos();

            Quantity productQuantity = product.getQuantity();

            for (SupplierInfo supplierInfo : supplierInfos) {
                OptimalMethod optimalMethod = new OptimalMethod();
                Supplier supplier = supplierInfo.getSupplier();

                double optimalOrder = Math.sqrt(2 * (product.getQuantity().getExpenses() * 90) * supplier.getDeliveryPrise());
                //Создаем новую переменную для расчета затрат на покупку
                double expensesIndicatorWithoutDiscount =
                        ((supplier.getDeliveryPrise() * (productQuantity.getExpenses() * 90)) / optimalOrder)
                                + (optimalOrder / 2)
                                + (supplierInfo.getSupplierPrice() * (productQuantity.getExpenses() * 90));
                //Создаем новую переменную для расчета затрат на покупку со скидкой
                double expensesIndicatorWithDiscount =
                        ((supplier.getDeliveryPrise() * (productQuantity.getExpenses() * 90)) / supplierInfo.getDiscountBorder())
                                + (supplierInfo.getDiscountBorder() / 2)
                                + (supplierInfo.getDiscountPrice() * (productQuantity.getExpenses() * 90));

                //Добавление всех данных в класс optimalMethod
                OffsetDateTime optimalDate = calculateOptimalDate(productQuantity.getPrice(), productQuantity.getExpenses(), supplier.getDeliveryDays());

                optimalMethod.setOptimalOrder(optimalOrder);
                optimalMethod.setExpensesIndicatorWithoutDiscount(expensesIndicatorWithoutDiscount);
                optimalMethod.setExpensesIndicatorWithDiscount(expensesIndicatorWithDiscount);
                optimalMethod.setOptimalOrderDate(optimalDate);

                optimalMethod.setProductName(product.getName());
                optimalMethod.setProductPrice(supplierInfo.getSupplierPrice());
                optimalMethod.setDiscountBorder(supplierInfo.getDiscountBorder());
                optimalMethod.setDiscountPrice(supplierInfo.getDiscountPrice());
                result.add(optimalMethod);
            }
        }

        return result;
    }

    private OffsetDateTime calculateOptimalDate(int price, int expenses, int deliveryDays) {
        OffsetDateTime result = OffsetDateTime.now();

        if (expenses == 0) {
            return null;
        }

        while (price <= 0) {
            price = price - expenses;
            result = result.plusDays(1);
        }

        return result.minusDays(deliveryDays);
    }
}
